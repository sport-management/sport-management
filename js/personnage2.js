//---------------------fonction qui permet de créer le  personnage -------------------------------------\\

function Character() {

    var self = this;

    //----------toutes les caractérisriques du personnage qui ne vont pas changer durant le jeu-------\\
    this.skinColor = Colors.peau;
    this.hairColor = Colors.brownDark;
    this.shirtColor = Colors.red;
    this.shortsColor = Colors.black;
    this.jumpDuration = 0.6;
    this.jumpHeight = 2000;
    this.speed = 100;
    this.energy;
    this.coeff = 0; //coeff mutliplicateur énergie
    this.signeCoeff;
    this.coeffup;
    this.coeffDown;
    this.init = 0;
    this.t = 0;
    this.bonus = 0;

    this.previousEnergy = sessionStorage.getItem("energieMarathon");

    document.getElementById("energy").style.width = this.previousEnergy + "%";

    document.getElementById("speed").getElementsByTagName("td")[1].style.backgroundColor = "rgb(0, 179, 15)"

    // -----------------------Initialisation du personnage--------------------------\\
    init();


    function init() {
        //-------------------------tous les paramètres graphiques------------------------\\
        self.face = createBox(100, 100, 60, self.skinColor, 0, 0, 0);
        self.hair = createBox(105, 20, 65, self.hairColor, 0, 50, 0);
        self.head = createGroup(0, 260, -25);
        self.head.add(self.face);
        self.head.add(self.hair);

        self.torso = createBox(150, 190, 40, self.shirtColor, 0, 100, 0);

        self.leftLowerArm = createLimb(20, 120, 30, self.skinColor, 0, -170, 0);
        self.leftArm = createLimb(30, 140, 40, self.skinColor, -100, 190, -10);
        self.leftArm.add(self.leftLowerArm);

        self.rightLowerArm = createLimb(
            20, 120, 30, self.skinColor, 0, -170, 0);
        self.rightArm = createLimb(30, 140, 40, self.skinColor, 100, 190, -10);
        self.rightArm.add(self.rightLowerArm);

        self.leftLowerLeg = createLimb(40, 200, 40, self.skinColor, 0, -200, 0);
        self.leftLeg = createLimb(50, 170, 50, self.shortsColor, -50, -10, 30);
        self.leftLeg.add(self.leftLowerLeg);

        self.rightLowerLeg = createLimb(
            40, 200, 40, self.skinColor, 0, -200, 0);
        self.rightLeg = createLimb(50, 170, 50, self.shortsColor, 50, -10, 30);
        self.rightLeg.add(self.rightLowerLeg);

        self.element = createGroup(0, 0, -4000);
        self.element.add(self.head);
        self.element.add(self.torso);
        self.element.add(self.leftArm);
        self.element.add(self.rightArm);
        self.element.add(self.leftLeg);
        self.element.add(self.rightLeg);

        //----------------tous les paramètres qui peuvent changer durant la partie----------------------\\
        self.isJumping = false;
        self.isSwitchingLeft = false;
        self.isSwitchingRight = false;
        self.isDecelerating = false;
        self.isAccelerating = false;
        self.currentLane = 0;
        self.runningStartTime = new Date() / 1000;
        self.pauseStartTime = new Date() / 1000;
        self.timer = new Timer();
        self.stepFreq = 2;
        self.queuedActions = [];

    }


    function createLimb(dx, dy, dz, color, x, y, z) {
        var limb = createGroup(x, y, z);
        var offset = -1 * (Math.max(dx, dz) / 2 + dy / 2);
        var limbBox = createBox(dx, dy, dz, color, 0, offset, 0);
        limb.add(limbBox);
        return limb;
    }


    this.update = function () {
        var currentTime = new Date() / 1000;


        //---------------sauvegarde des mouvement à appliquer si on appuie sur une touche--------------\\
        if (!self.isJumping &&
            !self.isSwitchingLeft &&
            !self.isSwitchingRight &&
            self.queuedActions.length > 0) {
            switch (self.queuedActions.shift()) {
                case "up":
                    self.isJumping = true;
                    self.jumpStartTime = new Date() / 1000;
                    break;
                case "left":
                    if (self.currentLane != -1) {
                        self.isSwitchingLeft = true;
                    }
                    break;
                case "right":
                    if (self.currentLane != 1) {
                        self.isSwitchingRight = true;
                    }
                    break;
                case "down":
                    self.isDecelerating = true;
                    break;

                case "shift":
                    self.isAccelerating = true;
                    break;
            }
        }

        //-------------------si le personnage saute on augmente sa hauteur-----------------------\\
        if (self.isJumping) {
            var jumpClock = currentTime - self.jumpStartTime;
            self.element.position.y = self.jumpHeight * Math.sin(
                (1 / self.jumpDuration) * Math.PI * jumpClock) +
                sinusoid(2 * self.stepFreq, 0, 20, 0,
                    self.jumpStartTime - self.runningStartTime);
            if (jumpClock > self.jumpDuration) {
                self.isJumping = false;
                self.runningStartTime += self.jumpDuration;
            }
        } else {
            //------permet de faire les effets quand le personnages court, il déplace les bras, les jambes---------\\
            var runningClock = currentTime - self.runningStartTime;
            self.element.position.y = sinusoid(
                2 * self.stepFreq, 0, 20, 0, runningClock);
            self.head.rotation.x = sinusoid(
                2 * self.stepFreq, -10, -5, 0, runningClock) * deg2Rad;
            self.torso.rotation.x = sinusoid(
                2 * self.stepFreq, -10, -5, 180, runningClock) * deg2Rad;
            self.leftArm.rotation.x = sinusoid(
                self.stepFreq, -70, 50, 180, runningClock) * deg2Rad;
            self.rightArm.rotation.x = sinusoid(
                self.stepFreq, -70, 50, 0, runningClock) * deg2Rad;
            self.leftLowerArm.rotation.x = sinusoid(
                self.stepFreq, 70, 140, 180, runningClock) * deg2Rad;
            self.rightLowerArm.rotation.x = sinusoid(
                self.stepFreq, 70, 140, 0, runningClock) * deg2Rad;
            self.leftLeg.rotation.x = sinusoid(
                self.stepFreq, -20, 80, 0, runningClock) * deg2Rad;
            self.rightLeg.rotation.x = sinusoid(
                self.stepFreq, -20, 80, 180, runningClock) * deg2Rad;
            self.leftLowerLeg.rotation.x = sinusoid(
                self.stepFreq, -130, 5, 240, runningClock) * deg2Rad;
            self.rightLowerLeg.rotation.x = sinusoid(
                self.stepFreq, -130, 5, 60, runningClock) * deg2Rad;

            //------------si le personnges change de rangée-------------------\\
            if (self.isSwitchingLeft) {
                self.element.position.x -= 200;
                var offset = self.currentLane * 800 - self.element.position.x;
                if (offset > 800) {
                    self.currentLane -= 1;
                    self.element.position.x = self.currentLane * 800;
                    self.isSwitchingLeft = false;
                }
            }
            if (self.isSwitchingRight) {
                self.element.position.x += 200;
                var offset = self.element.position.x - self.currentLane * 800;
                if (offset > 800) {
                    self.currentLane += 1;
                    self.element.position.x = self.currentLane * 800;
                    self.isSwitchingRight = false;
                }
            }
            //---------------------------si le personnage change de vitesse----------------------------\\
            if (self.isDecelerating) {

                if (this.speed > 80 && this.coeff >= 0) {
                    this.speed -= 5;
                    this.coeff--;
                    this.coeffDown = 1;
                    this.init = 1;
                }
                self.isDecelerating = false;
            }
            if (self.isAccelerating) {

                if (this.speed < 120) {
                    this.speed += 5;
                    this.coeff++;
                    this.coeffup = 1;
                    this.init = 1;
                }
                self.isAccelerating = false;
            }
        }
        //-----------------------------évolution de l'énergie---------------------------------\\
        if (this.init == 0) { // avant tout changement de vitesse

            if (this.bonus != 0) {
                this.previousEnergy = this.energy + this.bonus;
                this.timer.seconds = 1;
            }
            this.energy = this.previousEnergy - this.timer.seconds;
            this.signeCoeff = 1;
        }

        if (this.coeff > 0) { //si on augmente la vitesse
            if (this.signeCoeff != 2 || this.coeffup == 1 || this.coeffDown == 1) { //si le coeff n'était pas >0 au tour d'avant || si coeff++
                this.previousEnergy = this.energy;
                this.timer.seconds = 1;
            }
            if (this.bonus != 0) {
                this.previousEnergy = this.energy + this.bonus;
                this.timer.seconds = 1;
            }
            this.energy = this.previousEnergy - this.timer.seconds * (this.coeff + 1) + this.bonus;
            this.signeCoeff = 2;
            this.coeffup = 0;
            this.coeffDown = 0;
        }

        if (this.coeff == 0 && this.init != 0) { //quand on revient à la vitesse initiale
            if (this.signeCoeff != 1) {
                this.previousEnergy = this.energy;
                this.timer.seconds = 1;
            }
            if (this.bonus != 0) {
                this.previousEnergy = this.energy + this.bonus;
                this.timer.seconds = 1;
            }
            this.energy = this.previousEnergy - this.timer.seconds + this.bonus;
            this.signeCoeff = 1;
        }

        // si on ralentie 1 fois l'énergie reste inchangée
        if (this.coeff == -1) {
            if (this.bonus != 0) {
                this.previousEnergy = this.energy + this.bonus;
                this.energy = this.previousEnergy;
                this.timer.seconds = 1;
            }
        }

        if (this.energy <= 100) {
            if (this.coeff < -1) { //si on ralentie
                if (this.signeCoeff != 0 || this.coeffup == 1 || this.coeffDown == 1) { //si le coeff n'était pas <0 au tour d'avant || si coeff++
                    this.previousEnergy = this.energy;
                    this.timer.seconds = 1;
                }
                if (this.bonus != 0) {
                    this.previousEnergy = this.energy + this.bonus;
                    this.timer.seconds = 1;
                }
                this.energy = this.previousEnergy - this.timer.seconds * (this.coeff - 1) + this.bonus;
                this.signeCoeff = 0;
                this.coeffup = 0;
                this.coeffDown = 0;
            }
        }
        else if (this.bonus < 0) {
            this.previousEnergy = this.energy + this.bonus;
            this.energy = this.previousEnergy;
            this.timer.seconds = 1;
        }

        document.getElementById("energy").style.width = this.energy + "%";
        this.displaySpeed();
        this.bonus = 0;

        this.timer.update();

        if (this.t <= 0) {
            document.getElementById("panel1").style.backgroundColor = "rgb(187, 187, 187)";
        }
        else {
            document.getElementById("panel1").style.backgroundColor = "rgb(216, 7, 7, " + this.t + ")";
        }
        this.t -= 1;

        if (this.energy <= 0) {
            document.getElementById("panel1").style.backgroundColor = "rgb(216, 7, 7)";
        }
    }


    this.currentSpeed = function () {
        return this.speed;
    }

    this.displaySpeed = function () {

        document.getElementById("speed").getElementsByTagName("td")[this.coeff + 1].style.backgroundColor = "rgb(0, 179, 15)";
        if (this.coeff + 1 == 5) {
            document.getElementById("speed").getElementsByTagName("td")[this.coeff].style.backgroundColor = "rgba(0, 0, 0, 0)";
        }
        else if (this.coeff + 1 == 0) {

            document.getElementById("speed").getElementsByTagName("td")[this.coeff + 2].style.backgroundColor = "rgba(0, 0, 0, 0)";
        }
        else {
            document.getElementById("speed").getElementsByTagName("td")[this.coeff].style.backgroundColor = "rgba(0, 0, 0, 0)";
            document.getElementById("speed").getElementsByTagName("td")[this.coeff + 2].style.backgroundColor = "rgba(0, 0, 0, 0)";
        }
    }

    this.getEnergy = function () {
        return this.energy;
    }

    this.prime = function () {
        this.bonus = 9;
    }

    this.collisionCanette = function () {
        this.bonus = -1;
    }


    this.onLeftKeyPressed = function () {
        self.queuedActions.push("left");
    }


    this.onUpKeyPressed = function () {
        self.queuedActions.push("up");
    }


    this.onRightKeyPressed = function () {
        self.queuedActions.push("right");
    }


    this.onDownKeyPressed = function () {
        self.queuedActions.push("down");
    }

    this.onShiftKeyPressed = function () {
        self.queuedActions.push("shift");
    }


    this.onPause = function () {
        self.pauseStartTime = new Date() / 1000;
    }

    this.onUnpause = function () {
        var currentTime = new Date() / 1000;
        var pauseDuration = currentTime - self.pauseStartTime;
        self.runningStartTime += pauseDuration;
        if (self.isJumping) {
            self.jumpStartTime += pauseDuration;
        }
    }

}


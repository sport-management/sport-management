sessionStorage.setItem("sautHaie", 1);
sessionStorage.setItem("marathon", 0);

// Créez un nouveau monde lorsque la page est chargée
window.addEventListener('load', function () {
	new World();
});

let audio = new Audio("sound/music.mp3");


/*
	Fonction World() qui est une instance.
	Initialise le jeu et contient la boucle de jeu principale.
*/

function World() {

	//----------------------Toutes les variables du monde globales du monde----------------------------------\\
	var element, scene, camera, character, renderer, light,
		objects, paused, keysAllowed, score, difficulty, d,
		treePresenceProb, fogDistance, gameOver, distance, arriveehasSpawn;
	let timer = new Timer();
	//Initialisation
	init();

	//---------------initialiation de toutes les variables avant de commencer la partie-----------------------\\

	function init() {



		document.getElementById('bestScore').innerHTML += localStorage.getItem('bestScoreH')
		document.getElementById('bestPseudo').innerHTML += localStorage.getItem('bestPseudoH')

		if (localStorage.getItem('bestScoreH') != null) {
			document.getElementById('bestScore').style.visibility = "visible";
			document.getElementById('bestPseudo').style.visibility = "visible";
		}
		else {
			document.getElementById('bestScore').style.visibility = "hidden";
			document.getElementById('bestPseudo').style.visibility = "hidden";
		}


		element = document.getElementById('world');



		renderer = new THREE.WebGLRenderer({
			alpha: true, //enleve la couleur du fond
			antialias: true
		});
		renderer.setSize(element.clientWidth, element.clientHeight);
		renderer.shadowMap.enabled = true; //activation du brouillard
		element.appendChild(renderer.domElement);

		// Initialise la scene.
		// Brouillard
		scene = new THREE.Scene();
		fogDistance = 40000;
		scene.fog = new THREE.Fog(0xbadbe4, 1, fogDistance); // changement couleur du brouillard
		distance = 20000; //distance où la course se finit

		// ---------------------Initialise la camera avec le point de vue, aspect ratio,--------------------\\
		// Position de la camera
		camera = new THREE.PerspectiveCamera(
			60, element.clientWidth / element.clientHeight, 1, 120000); // parametre camera
		camera.position.set(0, 1500, -2000); // position de la camera
		camera.lookAt(new THREE.Vector3(0, 600, -5000)); // position de la direction de la camera
		window.camera = camera;
		arriveehasSpawn = false;
		window.addEventListener('resize', handleWindowResize, false);

		//Ajout de la lumiere
		light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
		scene.add(light);

		//-------------------initialise le personnage et l'ajoute à la scène---------------------------\\
		character = new Character();
		scene.add(character.element);


		var ground = createBox(2750, 20, 120000, Colors.piste, 0, -400, -60000);
		scene.add(ground);// Sol(piste)


		var decor = createBox(120000, 10, 120000, Colors.herbe, 0, -400, -60000);
		scene.add(decor);// sol (herbe)

		// --------------------------ligne de separation couloir-----------------------------------------\\
		var line = createBox(25, 20, 120000, Colors.white, 425, -400, -60000);
		var line1 = createBox(25, 20, 120000, Colors.white, -425, -400, -60000);
		var line2 = createBox(25, 20, 120000, Colors.white, 1250, -400, -60000);
		var line3 = createBox(25, 20, 120000, Colors.white, -1250, -400, -60000);
		scene.add(line);
		scene.add(line1);
		scene.add(line2);
		scene.add(line3);


		objects = [];
		treePresenceProb = 0.2;
		maxTreeSize = 0.5;
		for (var i = 10; i < 40; i++) {
			createRowOfBarrieresInitial(i * -3000, treePresenceProb, 0.5, 0.5);
			createSpectator(i * -3000, treePresenceProb, 1, 1);

		}



		//le jeu est en pause au début
		gameOver = false;
		paused = true;


		// ---------------------------------Parametrage des touches de commande---------------------------\\
		var left = 37;
		var up = 32;
		var right = 39;
		var down = 40;
		var shift = 38;
		var p = 80;
		var m =77;
		
		


		keysAllowed = {};
		document.addEventListener(
			'keydown',
			function (e) {
				if (!gameOver) {
					var key = e.keyCode;
					if (keysAllowed[key] === false) return;
					keysAllowed[key] = false;
					if (paused && !collisionsDetected() && key == up) { //CAS OPTION 1
						paused = false;
						character.onUnpause();
						document.getElementsByClassName(
							"variable-content")[0].style.visibility = "hidden";
						document.getElementById(
							"controls").style.display = "none";
					}
					else {
						if (key == p) {
							paused = true;
							character.onPause();
							document.getElementsByClassName(
								"variable-content")[0].style.visibility = "visible";
							document.getElementsByClassName(
								"variable-content")[0].innerHTML =
								"Jeu en pause, Appuyer sur espace pour reprendre.";
						}
						if (key == up && !paused) {
							character.onUpKeyPressed();
						}
						if (key == left && !paused) {
							character.onLeftKeyPressed();
						}
						if (key == right && !paused) {
							character.onRightKeyPressed();
						}
						if (key == down && !paused) {
							character.onDownKeyPressed();
						}
						if (key == shift && !paused) {
							character.onShiftKeyPressed();
						}
						if( key == m ){
							if(audio.paused){
								audio.play();
								
							}else{
								audio.pause();
								
							}
						}

					}
				}
			}
		);
		document.addEventListener(
			'keyup',
			function (e) {
				keysAllowed[e.keyCode] = true;
			}
		);
		document.addEventListener(
			'focus',
			function (e) {
				keysAllowed = {};
			}
		);

		// Initialisation du score et de la difficulté
		score = 0;
		difficulty = 0;
		document.getElementById("score").innerHTML = timer.getInGameTime();



		// commence la boucle du moteur de rendu
		loop();

	}

	//----------------------------------début de la boucle principale---------------------------------\\
	function loop() {
		//mise à jour du jeu
		if (!paused) {
			if (timer.numberBoucle % 21 == 0) {//permet de le faire 1 fois sur 21 pour qu'il n'y ai aps trop de barrières
				difficulty += 1;
				var levelLength = 30;
				if ((difficulty >= 5 * levelLength && difficulty < 6 * levelLength)) {
					fogDistance -= (25000 / levelLength);
				} else if (difficulty >= 8 * levelLength && difficulty < 9 * levelLength) {
					fogDistance -= (5000 / levelLength);
				}


				//------------------------ajout de tous les élément qui peuvent apparaîtres----------------------\\

				createArrivee(distance);
				createRowOfBarrieres(-120000, 0.2, 0.5, 0.5);
				createSpectator(-120000, treePresenceProb, 1, 1);
				scene.fog.far = fogDistance;
			}


			// Vitesse de déplacement des arbres
			objects.forEach(function (object) {
				object.mesh.position.z += character.currentSpeed();
			});

			// retire les objets qui passent derrière le joueur
			objects = objects.filter(function (object) {
				if (object.mesh.position.z > -3000) {
					scene.remove(object.mesh);
				}
				return object.mesh.position.z < -2000;
			});


			//fait bouger le personnage en fonction des inputs
			character.update();

			//---------------------regarde les collisions entre le personnage et les obstacles------------------\\

			if (collisionsDetected()) {
				character.penalty();
			}
			// -----------------------------------------------Fin de partie (Game Over)-------------------------------------\\
			if (score > distance || character.getEnergy() <= 0) {
				gameOver = true;
				paused = true;
				if (character.getEnergy() > 0) {
					saveScoreHaie(distance);
				}
				gameIsOver(score, "haies");
			}

			//mise à jour des scores et du temps
			score += character.currentSpeed() / 10;
			timer.update()
			document.getElementById("score").innerHTML = timer.getInGameTime();
		}

		//fait un rendu de la page en boucle.
		renderer.render(scene, camera);
		requestAnimationFrame(loop);
	}




	//----------------fait les changements nécessaires lorsqu'on change la taille de la fenêtre--------------\\
	function handleWindowResize() {
		renderer.setSize(element.clientWidth, element.clientHeight);
		camera.aspect = element.clientWidth / element.clientHeight;
		camera.updateProjectionMatrix();
	}
	//------------------------------créer l'arrivée seulement à une distance précise------------------------------\\
	function createArrivee(distance) {

		if (!arriveehasSpawn) {
			activateSpawnFinish = distance - ((distance % 3000) + 6000);
			if (score >= activateSpawnFinish) {
				arriveehasSpawn = true;
				var arrivee = new Arrivee(0, 0, -(6000 + distance % 3000) * 10 - 2000);
				objects.push(arrivee);
				scene.add(arrivee.mesh);
			}
		}
	}
	//-------------------------------créer les barrières à l'initialisation du jeu-------------------------------\\
	function createRowOfBarrieresInitial(position, probability, minScale, maxScale) {
		for (var lane = -1; lane < 2; lane++) {
			var randomNumber = Math.random();
			if (randomNumber < probability) {
				var scale = minScale + (maxScale - minScale) * Math.random();
				var barriere = new Barriere(lane * 800, -400, position, scale);
				objects.push(barriere);
				scene.add(barriere.mesh);
			}
		}
	}
	//-------------------------------créer les barrières dès que le personnage se déplace-------------------------------\\
	function createRowOfBarrieres(position, probability, minScale, maxScale) {
		for (var lane = -1; lane < 2; lane++) {
			var randomNumber = Math.random();
			if (randomNumber < probability) {

				if (score < distance) {

					var scale = minScale + (maxScale - minScale) * Math.random();
					var barriere = new Barriere(lane * 800, -400, position, scale);
					objects.push(barriere);
					scene.add(barriere.mesh);
				}
			}
		}
	}

	//-------------------------------création des spéctateurs sur le côté de la piste---------------------------------------\\
	function createSpectator(position, probability, minScale, maxScale) {
		var lane = 2;
		var randomNumber = Math.random();
		if (randomNumber < probability) {
			var scale = minScale + (maxScale - minScale) * Math.random();
			var rotate = 120 * deg2Rad;
			var spec = new Spectator(lane * 800, -400, position, scale, rotate);
			objects.push(spec);
			scene.add(spec.mesh);
		}

		lane = -2;
		var randomNumber = Math.random();
		if (randomNumber < probability) {
			var scale = minScale + (maxScale - minScale) * Math.random();
			var rotate = -120 * deg2Rad;
			var spec = new Spectator(lane * 800, -400, position, scale, rotate);
			objects.push(spec);
			scene.add(spec.mesh);
		}
	}


	// -------------------true si le coureur rentre en collision avec des objets---------------------------\\
	function collisionsDetected() {
		var charMinX = character.element.position.x - 115;
		var charMaxX = character.element.position.x + 115;
		var charMinY = character.element.position.y - 310;
		var charMaxY = character.element.position.y + 320;
		var charMinZ = character.element.position.z - 40;
		var charMaxZ = character.element.position.z + 40;
		for (var i = 0; i < objects.length; i++) {
			if (objects[i].collides(charMinX, charMaxX, charMinY,
				charMaxY, charMinZ, charMaxZ)) {
				return true;
			}
		}
		return false;
	}

}

function createGroup(x, y, z) {
	var group = new THREE.Group();
	group.position.set(x, y, z);
	return group;
}

//------------------------------création d'un rectangle----------------------------------\\
function createBox(dx, dy, dz, color, x, y, z, notFlatShading) {
	var geom = new THREE.BoxGeometry(dx, dy, dz);
	var mat = new THREE.MeshPhongMaterial({
		color: color,
		flatShading: notFlatShading != true
	});
	var box = new THREE.Mesh(geom, mat);
	box.castShadow = true;
	box.receiveShadow = true;
	box.position.set(x, y, z);
	return box;
}



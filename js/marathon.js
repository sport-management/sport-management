sessionStorage.setItem("sautHaie", 0);
sessionStorage.setItem("marathon", 1);
let audio = new Audio("sound/music.mp3");

//----------------------Créez un nouveau monde lorsque la page est chargée------------------------\\
window.addEventListener('load', function () {
	new World();
});




	//----------------Initialise le jeu et contient la boucle de jeu principale---------------------\\.


function World() {

	let self = this;
	let varheure = 50000;
	let brouillardAvance = true;
	let distancePopItem = 480
	distance = 42000;

	valeur = 0;
	heure = 0;

	//variables du monde
	var element, scene, camera, character, renderer, light,
		objects, paused, keysAllowed, score, difficulty,
		nbreBoucle, fogDistance, gameOver, distance;
	let timer = new Timer()
	//---------------------------------------Initialisation----------------------------------------\\
	init();

	//Création du moteur de rendu, de la scène, la lumière, de la caméra et du coureur

	function init() {

		document.getElementById('bestScore').innerHTML += localStorage.getItem('bestScoreM')
		document.getElementById('bestPseudo').innerHTML += localStorage.getItem('bestPseudoM')
		element = document.getElementById('world');

		if (localStorage.getItem('bestScoreM') != null) {
			document.getElementById('bestScore').style.visibility = "visible";
			document.getElementById('bestPseudo').style.visibility = "visible";
		}
		else {
			document.getElementById('bestScore').style.visibility = "hidden";
			document.getElementById('bestPseudo').style.visibility = "hidden";
		}

		renderer = new THREE.WebGLRenderer({
			alpha: true, //enleve la couleur du fond
			antialias: true
		});
		renderer.setSize(element.clientWidth, element.clientHeight);
		renderer.shadowMap.enabled = true; //activation du brouillard
		element.appendChild(renderer.domElement);

		// ------------------------Initialise la scene/Brouillard.---------------------------\\
		scene = new THREE.Scene();
		fogDistance = 40000;
		fogday = new THREE.Fog(0xbadbe4, 1, fogDistance); // changement couleur du brouillard
		scene.fog = fogday // choix brouillard

		//----------------------------------- Position de la camera--------------------------------\\
		camera = new THREE.PerspectiveCamera(
			60, element.clientWidth / element.clientHeight, 1, 120000); // parametre camera
		camera.position.set(0, 1500, -2000); // position de la camera
		camera.lookAt(new THREE.Vector3(0, 600, -5000)); // position de la direction de la camera
		window.camera = camera;

		window.addEventListener('resize', handleWindowResize, false);

		//Ajout de la lumiere
		light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
		lightnight = new THREE.HemisphereLight(0xffffff, 0x000000, 0.5);
		scene.add(light);

		//initialise le personnage et l'ajoute à la scène.
		character = new Character();
		scene.add(character.element);

		// Sol(piste)
		var ground = createBox(2750, 20, 120000, Colors.piste, 0, -400, -60000);
		scene.add(ground);

		// sol (herbe)
		var decor = createBox(120000, 10, 120000, Colors.herbe, 0, -400, -60000);
		scene.add(decor);


		objects = [];
		PresenceProb = 0.2;
		nbreBoucle = 0;
		for (var i = 10; i < 40; i++) {
			createTree(i * -3000, PresenceProb, 2, 2)
		}
		createTable(PresenceProb);
		createSpectator(-120000, 1)

		//------------------------------------le jeu est en pause au début-------------------------------\\
		gameOver = false;
		paused = true;
		arriveehasSpawn = false;

		//-------------------------------Parametrage des touches de commande-------------------------------\\
		var left = 37;
		var up = 32;
		var right = 39;
		var down = 40;
		var shift = 38;
		var p = 80;
		var m =77;

		keysAllowed = {};
		document.addEventListener(
			'keydown',
			function (e) {
				if (!gameOver) {
					var key = e.keyCode;
					if (keysAllowed[key] === false) return;
					keysAllowed[key] = false;
					if (paused && !collisionsDetected() && key == up) { //CAS OPTION 1
						paused = false;
						character.onUnpause();
						document.getElementsByClassName(
							"variable-content")[0].style.visibility = "hidden";
						document.getElementsByClassName(
							"variable-content")[1].style.visibility = "hidden";
						document.getElementById(
							"controls").style.display = "none";
					}
					else {
						if (key == p) {
							paused = true;
							character.onPause();
							document.getElementsByClassName(
								"variable-content")[0].style.visibility = "visible";
							document.getElementsByClassName(
								"variable-content")[0].innerHTML =
								"Jeu en pause, Appuyer sur espace pour reprendre.";
						}
						if (key == up && !paused) {
							character.onUpKeyPressed();
						}
						if (key == left && !paused) {
							character.onLeftKeyPressed();
						}
						if (key == right && !paused) {
							character.onRightKeyPressed();
						}
						if (key == down && !paused) {
							character.onDownKeyPressed();
						}
						if (key == shift && !paused) {
							character.onShiftKeyPressed();
						}
						if( key == m ){
							if(audio.paused){
								audio.play();

							}else{
								audio.pause();

							}
						}


					}
				}
			}
		);
		document.addEventListener(
			'keyup',
			function (e) {
				keysAllowed[e.keyCode] = true;
			}
		);
		document.addEventListener(
			'focus',
			function (e) {
				keysAllowed = {};
			}
		);

	//----------------Initialisation du score et de la difficulté------------------\\
		score = 0;
		difficulty = 0;
		document.getElementById("score").innerHTML = score;
		document.getElementById("timer").innerHTML = timer.getInGameTime();
		//music.play();


		// commence la boucle du moteur de rendu
		loop();

	}

	//-----------------------début de la boucle principale-----------------------------\\
	function loop() {
		//mise à jour du jeu
		if (!paused) {
			nbreBoucle++;
			if (nbreBoucle % 30 == 0) {
				var levelLength = 30;

				if (brouillardAvance == true) {
					difficulty += 1;
					if ((difficulty >= 5 * levelLength && difficulty < 6 * levelLength)) {
						fogDistance -= (25000 / levelLength);
					} else if (difficulty >= 8 * levelLength && difficulty < 9 * levelLength) {
						fogDistance -= (5000 / levelLength);
					}
					if (difficulty == 9 * levelLength) {
						brouillardAvance = false;
					}
				}
				else {
					difficulty -= 1;
					if ((difficulty >= 5 * levelLength && difficulty < 6 * levelLength)) {
						fogDistance += (25000 / levelLength);
					} else if (difficulty >= 8 * levelLength && difficulty < 9 * levelLength) {
						fogDistance += (5000 / levelLength);
					}
					if (difficulty == 0) {
						brouillardAvance = true;
					}
				}

				createTree(-120000, 0.2, 2, 2);
				createArrivee(distance);
				createTable(0.1)
				createSpectator(-120000, 1)
				scene.fog.far = fogDistance;
			}

			// Vitesse de déplacement des arbres
			objects.forEach(function (object) {
				object.mesh.position.z += character.currentSpeed();
			});

			// retire les objets qui passent derrière le joueur
			objects = objects.filter(function (object) {
				if (object.mesh.position.z > -3000) {
					scene.remove(object.mesh);
				}
				return object.mesh.position.z < -2000;
			});


			//------------------fait bouger le personnage en fonction des inputs----------------------\\
			character.update();


			//--------------regarde les collisions entre le personnage et les obstacles-----------------------\\
			// Fin de partie (Game Over)
			if (collisionsDetected() == "bouteille") {
				character.prime();
			}
			if (collisionsDetected() == "canette") {
				character.collisionCanette();
			}
			if (score > distance || character.energy <= 0) {
				gameOver = true;
				paused = true;
				if (character.getEnergy() > 0) {
					saveScoreMarathon();
				}
				gameIsOver(score, "marathon");
			}

			if (score % varheure == 0 && score != 0) {
				changementHeure()
			}

			//mise à jour des scores
			score += character.currentSpeed() / 10;
			document.getElementById("score").innerHTML = score;

			if(nbreBoucle == 600){
				nbreBoucle = 0;
			}
		timer.update()
		document.getElementById("timer").innerHTML = timer.getInGameTime();
		}

		//fait un rendu de la page en boucle.
		renderer.render(scene, camera);
		requestAnimationFrame(loop);
	}



	//---------fait les changements nécessaire lorsqu'on change la taille de la fenêtre-------------\\
	function handleWindowResize() {
		renderer.setSize(element.clientWidth, element.clientHeight);
		camera.aspect = element.clientWidth / element.clientHeight;
		camera.updateProjectionMatrix();
	}

	function createTable(probability) {
		var lane = 2;
		var randomNumber = Math.random();
		if (nbreBoucle % distancePopItem == 0) {
			var table = new Table(lane * 800, 0, -120000);
			for (i = 0; i < 200; i = i + 100) {
				for (j = 0; j < 300; j = j + 90) {
					var bouteille = new Bouteille((lane * 800) - 200 + i, 300, -120000 + j)
					objects.push(bouteille)
					scene.add(bouteille.mesh);
				}
			}
			objects.push(table);
			scene.add(table.mesh);

			createBonus(-120000);
		}
	}


	function createSpectator(position, scale) {
		var lane = 2;
		if (nbreBoucle % distancePopItem == 0) {
			var rotate = 90 * deg2Rad;
			var spec = new Spectator((lane * 800) + 500, -400, position, scale, rotate);
			objects.push(spec);
			scene.add(spec.mesh);
		}
	}

	function createCanette(lane, position) {
		var bouteille = new Canette((lane * 800), -350, position);
		objects.push(bouteille);
		scene.add(bouteille.mesh);
	}

	function createBouteille(lane, position) {
		var canette = new Bouteille((lane * 800), -350, position);
		objects.push(canette);
		scene.add(canette.mesh);
	}

	function createBonus(position) {
		randLane = Math.floor(Math.random() * (3)) - 1;
		if (randLane == -1) {
			createBouteille(-1, position);
			createCanette(0, position)
			createCanette(1, position)
		}
		if (randLane == 0) {
			createBouteille(0, position);
			createCanette(-1, position)
			createCanette(1, position)
		}
		if (randLane == 1) {
			createBouteille(1, position);
			createCanette(-1, position)
			createCanette(0, position)
		}
	}


	function createTree(position, probability, minScale, maxScale) {
		for (var lane = 5; lane <= 7; lane++) {
			var randomNumber = Math.random();
			if (randomNumber < probability) {
				var scale = minScale + (maxScale - minScale) * Math.random();
				var tree = new Tree(lane * 800, -400, position, scale);
				objects.push(tree);
				scene.add(tree.mesh);
			}
		}
		for (var lane = -7; lane <= -5; lane++) {
			var randomNumber = Math.random();
			if (randomNumber < probability) {
				var scale = minScale + (maxScale - minScale) * Math.random();
				var tree = new Tree(lane * 800, -400, position, scale);
				objects.push(tree);
				scene.add(tree.mesh);
			}
		}
	}
	function createArrivee(distance) {

		if (!arriveehasSpawn) {

			activateSpawnFinish = distance - ((distance % 3000) + 6000);
			if (score >= activateSpawnFinish) {
				arriveehasSpawn = true;
				var arrivee = new Arrivee(0, 0, -(6000 + distance % 3000) * 10 - 1300);
				objects.push(arrivee);
				scene.add(arrivee.mesh);
			}
		}
	}


	//--------------- Retour des collision si le coureur rentre en collision avec des objets----------------\\
	function collisionsDetected() {
		var charMinX = character.element.position.x - 115;
		var charMaxX = character.element.position.x + 115;
		var charMinY = character.element.position.y - 310;
		var charMaxY = character.element.position.y + 320;
		var charMinZ = character.element.position.z - 40;
		var charMaxZ = character.element.position.z + 40;
		for (var i = 0; i < objects.length; i++) {
			if (objects[i].collides(charMinX, charMaxX, charMinY,
				charMaxY, charMinZ, charMaxZ)) {
				return true;
			}
			if (objects[i].collidesC(charMinX, charMaxX, charMinY,
				charMaxY, charMinZ, charMaxZ)) {
				return "canette";
			}
			if (objects[i].collidesB(charMinX, charMaxX, charMinY,
				charMaxY, charMinZ, charMaxZ)) {
				return "bouteille";
			}
		}
		return false;
	}

	function changementHeure() {
		if (heure == 0) {
			valeur = valeur + 1;
		}
		else {
			valeur = valeur - 1;
		}
		scene.remove(light)
		switch (valeur) {
			case 0:
				fogday = new THREE.Fog(0xbadbe4, 1, fogDistance); // changement couleur du brouillard
				light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
				heure = 0;
				break;
			case 1:
				fogday = new THREE.Fog(0xafc6cd, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0xe9e9e9, 0.9);
				break;
			case 2:
				fogday = new THREE.Fog(0xb5bec1, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0xd6d6d6, 0.8);
				break;
			case 3:
				fogday = new THREE.Fog(0xa7a8a9, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0xbdbdbd, 0.7);
				break;
			case 4:
				fogday = new THREE.Fog(0x8c8c8c, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0x9e9e9e, 0.7);
				break;
			case 5:
				fogday = new THREE.Fog(0x5d5d5d, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0x7c7c7c, 0.6);
				break;
			case 6:
				fogday = new THREE.Fog(0x404040, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0x5d5d5d, 0.6);
				break;
			case 7:
				fogday = new THREE.Fog(0x1d1d1d, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0x2e2e2e, 0.5);
				break;
			case 8:
				fogday = new THREE.Fog(0x000000, 1, fogDistance);
				light = new THREE.HemisphereLight(0xffffff, 0x000000, 0.5);
				heure = 1;
				break;
		}

		scene.add(light);
		scene.fog = fogday;
		if (valeur == 0 || valeur == 8) {
			varheure = 50000
		}
		else {
			varheure = 5000
		}
	}

}



function createGroup(x, y, z) {
	var group = new THREE.Group();
	group.position.set(x, y, z);
	return group;
}

function createBox(dx, dy, dz, color, x, y, z, notFlatShading) {
	var geom = new THREE.BoxGeometry(dx, dy, dz);
	var mat = new THREE.MeshPhongMaterial({
		color: color,
		flatShading: notFlatShading != true
	});
	var box = new THREE.Mesh(geom, mat);
	box.castShadow = true;
	box.receiveShadow = true;
	box.position.set(x, y, z);
	return box;
}

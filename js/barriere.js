//------------------------------Création des haies graphiquement------------------------------------\\
function Barriere(x, y, z, s){

    var self = this;
    this.mesh = new THREE.Object3D();
    var left = createCylinder(75, 75, 1000, 32,Colors.red, -500, 500, 0);//poteau gauche
    var right = createCylinder(75, 75, 1000, 32, Colors.red, 500, 500, 0);//poteau droit
    var top = createCylinder(75, 75, 1000, 32, Colors.white, 0, 900, 0);//barre centrale
    top.rotation.z = Math.PI / 2;
    this.mesh.add(left);
    this.mesh.add(right);
    this.mesh.add(top);
    this.mesh.position.set(x,y,z)
    this.mesh.scale.set(s, s, s);
	this.scale = s;
//-------------------------------ajout des collisions de la barrière--------------------------------\\
    this.collides = function(minX, maxX, minY, maxY, minZ, maxZ) {
        var meshMinX = self.mesh.position.x - this.scale * 250;
        var meshMaxX = self.mesh.position.x;
        var meshMaxY = self.mesh.position.y + this.scale * 1150;
        var meshMinZ = self.mesh.position.z - this.scale * 250;
        var meshMaxZ = self.mesh.position.z + this.scale * 250;
        return (meshMinX <= maxX && meshMaxX >= minX
            && meshMaxY >= minY
            && meshMinZ <= maxZ && meshMaxZ >= minZ)
    }
}
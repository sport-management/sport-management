class Timer{
    constructor(){
        this.numberBoucle = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.hours = 0;
    }
    update(){
        //---------------nous utilisons le "In game Time" comme c'est un jeu ou le but est-----------------\\
    //d'arriver le plus vite à l'arrivée ainsi le temps du joueur ne dépend pas de la puissance du Pc------\\
    //------------------------------------------du joueur--------------------------------------------------\\
       this.numberBoucle ++
        if(this.numberBoucle == 62){
            this.seconds++;
            this.numberBoucle =0;
            if(this.seconds==60){
                this.minutes++
                this.seconds= 0;
                if(this.minutes==60){
                    this.hours+=1
                    this.minutes = 0
                }
            }
        }
    }
    //---------------------------retourne du texte pour pouvoir l'insérer en HTML--------------------------\\
    getInGameTime(){
        let seconds = this.seconds
        let minutes = this.minutes;
        let hours = this.hours;

        if(this.seconds<10)seconds = "0"+seconds;   
        if(this.minutes<10)minutes = "0"+minutes;
        if(this.hours<10)hours = "0"+hours;
        return hours+":"+minutes+":"+seconds
    }
}
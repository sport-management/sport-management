function storageHaie() {
  let objectPetitdej = document.getElementById('petitdej')
  let objectMidi = document.getElementById('midi')
  let objectChaussure = document.getElementById("chaussure");
  let objectUrine = document.getElementById("urine");
  let objectPseudo = document.getElementById("pseudo");
  let repasVeille = 0;
  let petitDej = 0;
  let repasDuMidi = 0;
  let chaussurePointe = 0;
  let encas = 0;
  let total = 0;

  sessionStorage.setItem("pseudoStorage", objectPseudo.value);

  //----------------------------------Condition repas de la veille-------------------------//
  if (document.getElementById("tartiflette").checked) {
    repasVeille -= 12;
  }
  if (document.getElementById("riz").checked) {
    repasVeille -= 4;
  }

  if (document.getElementById("oignon").checked) {
    repasVeille -= 4;
  }
  if (document.getElementById("yaourt").checked) {
    repasVeille += 4;
  }

  if (document.getElementById("pain").checked) {
    repasVeille += 4;
  }
  if (document.getElementById("pates").checked) {
    repasVeille += 12;
  }

  //----------------------------------------------------------------------------------------//

  //----------------------------------Condition petit dejeuner-------------------------//
  if (objectPetitdej.options[objectPetitdej.selectedIndex].value == "grasse_plus") {
    petitDej -= 20;
  }
  if (objectPetitdej.options[objectPetitdej.selectedIndex].value == "feculent_plus") {
    petitDej += 20;
  }
  if (objectPetitdej.options[objectPetitdej.selectedIndex].value == "grasse_moins") {
    petitDej -= 20;
  }
  if (objectPetitdej.options[objectPetitdej.selectedIndex].value == "feculent_moins") {
    petitDej -= 20;
  }

  //----------------------------------------------------------------------------------------//



  //----------------------------------Condition repas du midi en fonction de l'heure-------------------------//
  if (objectMidi.options[objectMidi.selectedIndex].value == "uneHeure") {
    repasDuMidi -= 20;
  }
  if (objectMidi.options[objectMidi.selectedIndex].value == "deuxHeure") {
    repasDuMidi -= 20;
  }
  if (objectMidi.options[objectMidi.selectedIndex].value == "troisHeure") {
    repasDuMidi += 20;
  }
  if (objectMidi.options[objectMidi.selectedIndex].value == "quatreHeure") {
    repasDuMidi -= 20;
  }
  //----------------------------------------------------------------------------------------//


  //----------------------------------Condition chaussures-------------------------//
  if (objectChaussure.options[objectChaussure.selectedIndex].value == "sans_pointe") {
    chaussurePointe -= 20;
  }
  if (objectChaussure.options[objectChaussure.selectedIndex].value == "pointe") {
    chaussurePointe += 20;
  }
  //----------------------------------------------------------------------------------------//


  //----------------------------------Condition urine-------------------------//

  if (objectUrine.options[objectUrine.selectedIndex].value == "jaune") {
    urine -= 20;
  }
  if (objectUrine.options[objectUrine.selectedIndex].value == "claire") {
    urine += 20;
  }

  //----------------------------------------------------------------------------------------//

  //----------------------------------Condition encas-------------------------//
  if (document.getElementById("redbull").checked) {
    encas -= 10;
  }
  if (document.getElementById("chocolat").checked) {
    encas -= 10;
  }
  if (document.getElementById("biscuit").checked) {
    encas += 10;
  }
  if (document.getElementById("eau").checked) {
    encas += 10;
  }

  //----------------------------------------------------------------------------------------//

  total = encas + repasDuMidi + repasVeille + chaussurePointe + petitDej;

  if (total < 20) {
    sessionStorage.setItem("energie", 10);
  }
  if (total >= 20 && total < 30) {
    sessionStorage.setItem("energie", 20);
  }
  if (total >= 30 && total < 40) {
    sessionStorage.setItem("energie", 30);
  }
  if (total >= 40 && total < 50) {
    sessionStorage.setItem("energie", 40);
  }
  if (total >= 50 && total < 60) {
    sessionStorage.setItem("energie", 50);
  }
  if (total >= 60 && total < 70) {
    sessionStorage.setItem("energie", 60);
  }
  if (total >= 70 && total < 80) {
    sessionStorage.setItem("energie", 70);
  }
  if (total >= 80 && total <= 90) {
    sessionStorage.setItem("energie", 80);
  }
  if (total >= 90 && total < 100) {
    sessionStorage.setItem("energie", 90);
  }
  if (total == 100) {
    sessionStorage.setItem("energie", 100);
  }




  sessionStorage.setItem("repasVeille", repasVeille);
  sessionStorage.setItem("petitDej", petitDej);
  sessionStorage.setItem("repasDuMidi", repasDuMidi);
  sessionStorage.setItem("chaussurePointe", chaussurePointe);
  sessionStorage.setItem("encas", encas);



}
















function storageMarathon() {
  let objectPseudo = document.getElementById("pseudo");
  sessionStorage.setItem("pseudoStorage", objectPseudo.value);

  let objectPetitdejMarathon = document.getElementById('petitDejMarathon')
  let objectBoissonAttente = document.getElementById('attente')
  let objectRecuperation = document.getElementById("recuperation");
  let semainePrec = 0;
  let petitDejMarathon = 0;
  let boissonAttente = 0;
  let entrainement = 0;
  let ravitaillement = 0;
  let recuperation = 0;
  let total = 0;

  sessionStorage.setItem("pseudoStorage", objectPseudo.value);

  //----------------------------------Condition semaine prec-------------------------//
  if (document.getElementById("glucide").checked) {
    semainePrec += 10;
  }
  if (document.getElementById("sucre").checked) {
    semainePrec -= 10;
  }

  if (document.getElementById("painMarathon").checked) {
    semainePrec += 10;
  }
  if (document.getElementById("ration").checked) {
    semainePrec -= 10;
  }

  //----------------------------------------------------------------------------------------//

  //----------------------------------Condition petit dejeuner marathon-------------------------//
  if (objectPetitdejMarathon.options[objectPetitdejMarathon.selectedIndex].value == "glucideHydra") {
    petitDejMarathon += 20;
  }
  if (objectPetitdejMarathon.options[objectPetitdejMarathon.selectedIndex].value == "lipideAlcool") {
    petitDejMarathon -= 20;
  }
  if (objectPetitdejMarathon.options[objectPetitdejMarathon.selectedIndex].value == "proteineHydra") {
    petitDejMarathon -= 20;
  }
  if (objectPetitdejMarathon.options[objectPetitdejMarathon.selectedIndex].value == "glucideAlcool") {
    petitDejMarathon -= 20;
  }
  if (objectPetitdejMarathon.options[objectPetitdejMarathon.selectedIndex].value == "proteineAlcool") {
    petitDejMarathon -= 20;
  }
  if (objectPetitdejMarathon.options[objectPetitdejMarathon.selectedIndex].value == "lipideHydra") {
    petitDejMarathon -= 20;
  }

  //----------------------------------------------------------------------------------------//



  //----------------------------------Condition boisson d'attente-------------------------//
  if (objectBoissonAttente.options[objectBoissonAttente.selectedIndex].value == "attentePlus") {
    boissonAttente -= 10;
  }
  if (objectBoissonAttente.options[objectBoissonAttente.selectedIndex].value == "feculent_plus") {
    boissonAttente += 10;
  }
  if (objectBoissonAttente.options[objectBoissonAttente.selectedIndex].value == "attenteMoins") {
    boissonAttente -= 10;
  }
  if (objectBoissonAttente.options[objectBoissonAttente.selectedIndex].value == "feculent_moins") {
    boissonAttente -= 10;
  }
  //----------------------------------------------------------------------------------------//


  //----------------------------------Condition pour entrainement-------------------------//
  if (document.getElementById("pasEntrainement").checked) {
    entrainement -= 10;
  }
  if (document.getElementById("fondamentale").checked) {
    entrainement += 5;
  }

  if (document.getElementById("frac").checked) {
    entrainement += 5;
  }
  
  


  //----------------------------------Condition recuperation-------------------------//

  if (objectRecuperation.options[objectRecuperation.selectedIndex].value == "hyperLipide") {
    recuperation -= 20;
  }
  if (objectRecuperation.options[objectRecuperation.selectedIndex].value == "hyperGlucide") {
    recuperation += 20;
  }
  if (objectRecuperation.options[objectRecuperation.selectedIndex].value == "hyperProteine") {
    recuperation -= 20;
  }

  //----------------------------------------------------------------------------------------//

  //----------------------------------Condition ravitaillement-------------------------//
  if (document.getElementById("rehydratation").checked) {
    ravitaillement += 10;
  }
  if (document.getElementById("abandonner").checked) {
    ravitaillement -= 10;
  }
  if (document.getElementById("stockLipide").checked) {
    ravitaillement -= 10;
  }
  if (document.getElementById("stockGlucide").checked) {
    ravitaillement += 10;
  }

  //----------------------------------------------------------------------------------------//

  total = semainePrec + petitDejMarathon + entrainement + boissonAttente + ravitaillement + recuperation;


  if (total < 20) {
    sessionStorage.setItem("energieMarathon", 10);
  }
  if (total >= 20 && total < 30) {
    sessionStorage.setItem("energieMarathon", 20);
  }
  if (total >= 30 && total < 40) {
    sessionStorage.setItem("energieMarathon", 30);
  }
  if (total >= 40 && total < 50) {
    sessionStorage.setItem("energieMarathon", 40);
  }
  if (total >= 50 && total < 60) {
    sessionStorage.setItem("energieMarathon", 50);
  }
  if (total >= 60 && total < 70) {
    sessionStorage.setItem("energieMarathon", 60);
  }
  if (total >= 70 && total < 80) {
    sessionStorage.setItem("energieMarathon", 70);
  }
  if (total >= 80 && total <= 90) {
    sessionStorage.setItem("energieMarathon", 80);
  }
  if (total >= 90 && total < 100) {
    sessionStorage.setItem("energieMarathon", 90);
  }
  if (total == 100) {
    sessionStorage.setItem("energieMarathon", 100);
  }




  sessionStorage.setItem("semainePrec", semainePrec);
  sessionStorage.setItem("petitDejMarathon", petitDejMarathon);
  sessionStorage.setItem("entrainement", entrainement);
  sessionStorage.setItem("boissonAttente", boissonAttente);
  sessionStorage.setItem("ravitaillement", ravitaillement);
  sessionStorage.setItem("recuperation", recuperation);





}








function checkStorage(choix) {
  if (typeof localStorage != 'undefined') {
    if (choix == 0) {
      storageHaie();
      document.location.href = "haie.html";
    }
    else if (choix == 1) {
      storageMarathon();
      document.location.href = "marathon.html";
    }
    else {
      document.location.href = "form.html";
    }
  } else {
    alert("localStorage n'est pas supporté");
  }
}




// -----------------------On définit toute les couleurs nécessaire------------------------------\\
var Colors = {
	cherry: 0xe35d6a,
	blue: 0x1560bd,
	white: 0xd8d0d1,
	black: 0x000000,
	brown: 0x59332e,
	peach: 0xffdab9,
	yellow: 0xffff00,
	olive: 0x556b2f,
	grey: 0x696969,
	sand: 0xc2b280,
	brownDark: 0x23190f,
	green: 0x669900,
	piste: 0x902F1F,
	herbe: 0x337600,
	peau: 0xFFD77C,
	red: 0xFF0000,
	blond: 0xFFF639
};

var deg2Rad = Math.PI / 180;
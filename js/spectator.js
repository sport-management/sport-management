//------------------------fonction qui permet de générer des spectateurs sur les côtés---------------------\\
function Spectator(pos_x, pos_y, pos_z, s, rotate) { 
	var self = this;

	this.skinColor = Colors.peau;
	this.hairColor = Colors.brownDark;
	this.shirtColor = Colors.white;
	this.shortsColor = Colors.brownDark;

	// ------------------------------Modification du personnage graphique------------------------------\\
	self.face = createBox(100, 100, 60, self.skinColor, 0, 0, 0);
	self.hair = createBox(105, 20, 65, self.hairColor, 0, 50, 0);
	self.head = createGroup(0, 260, 0);
	self.head.add(self.face);
	self.head.add(self.hair);

	self.torso = createBox(150, 190, 40, self.shirtColor, 0, 100, 0);

	self.leftLowerArm = createLimb(20, 120, 30, self.skinColor, 0, -170, 0);
	self.leftArm = createLimb(30, 140, 40, self.shirtColor, -100, 190, 0);
	self.leftArm.add(self.leftLowerArm);

	self.rightLowerArm = createLimb(20, 120, 30, self.skinColor, 0, -170, 0);
	self.rightArm = createLimb(30, 140, 40, self.shirtColor, 100, 190, 0);
	self.rightArm.add(self.rightLowerArm);

	self.leftLowerLeg = createLimb(40, 200, 40, self.shortsColor, 0, -200, 0);
	self.leftLeg = createLimb(50, 170, 50, self.shortsColor, -50, -10, 0);
	self.leftLeg.add(self.leftLowerLeg);

	self.rightLowerLeg = createLimb(40, 200, 40, self.shortsColor, 0, -200, 0);
	self.rightLeg = createLimb(50, 170, 50, self.shortsColor, 50, -10, 0);
	self.rightLeg.add(self.rightLowerLeg);


	this.mesh = new THREE.Object3D();

	this.mesh.add(self.head);
	this.mesh.add(self.torso);
	this.mesh.add(self.leftArm);
	this.mesh.add(self.rightArm);
	this.mesh.add(self.leftLeg);
	this.mesh.add(self.rightLeg);

	this.mesh.rotation.y = rotate;
	this.mesh.scale.set(s, s, s);
	this.mesh.position.set(pos_x, pos_y + 400, pos_z)


//---------------------ajout des collisions ---------------------------------------------\\
	this.collides = function(minX, maxX, minY, minZ, maxZ) {
        var meshMinX = self.mesh.position.pos_x - this.scale * 0;
        var meshMaxX = self.mesh.position.pos_x;
        var meshMaxY = self.mesh.position.pos_y + this.scale * 0;
        var meshMinZ = self.mesh.position.pos_z - this.scale * 0;
        var meshMaxZ = self.mesh.position.pos_z + this.scale * 0;
        return (meshMinX <= maxX && meshMaxX >= minX
            && meshMaxY >= minY
            && meshMinZ <= maxZ && meshMaxZ >= minZ)
    }
    this.collidesB = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesC = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
		return false;
	}

	function createLimb(dx, dy, dz, color, x, y, z) {
		var limb = createGroup(x, y, z);
		var offset = -1 * (Math.max(dx, dz) / 2 + dy / 2);
		var limbBox = createBox(dx, dy, dz, color, 0, offset, 0);
		limb.add(limbBox);
		return limb;
	}

	self.runningStartTime = new Date() / 1000;
	self.pauseStartTime = new Date() / 1000;

	this.update = function () {

		var currentTime = new Date() / 1000;

		var runningClock = (currentTime - self.runningStartTime) / 4;
		self.leftArm.rotation.pos_x = sinusoid(2, 0, 180, 180, runningClock) * deg2Rad;
		self.rightArm.rotation.pos_x = sinusoid(2, 0, 180, 180, runningClock) * deg2Rad;

	}
}
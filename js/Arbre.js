//-----------------------------Ajout des Arbres---------------------------\\
function Tree(x, y, z, s) {

	var self = this;
	this.mesh = new THREE.Object3D();
	var top = createCylinder(1, 300, 300, 4, Colors.green, 0, 1000, 0);
	var mid = createCylinder(1, 400, 400, 4, Colors.green, 0, 800, 0);
	var bottom = createCylinder(1, 500, 500, 4, Colors.green, 0, 500, 0);
	var trunk = createCylinder(100, 100, 250, 32, Colors.brownDark, 0, 125, 0);
	//--------------------ajout du tout à l'objet final--------------------------\\
	this.mesh.add(top);
	this.mesh.add(mid);
	this.mesh.add(bottom);
	this.mesh.add(trunk);
	this.mesh.position.set(x, y, z);
	this.mesh.scale.set(s, s, s);
	this.scale = s;

	//-------------------------ajout des collision des arbres-------------------------\\
	this.collides = function (minX, maxX, minY, maxY, minZ, maxZ) {
		var treeMinX = self.mesh.position.x - this.scale * 250;
		var treeMaxX = self.mesh.position.x + this.scale * 250;
		var treeMinY = self.mesh.position.y;
		var treeMaxY = self.mesh.position.y + this.scale * 1150;
		var treeMinZ = self.mesh.position.z - this.scale * 250;
		var treeMaxZ = self.mesh.position.z + this.scale * 250;
		return treeMinX <= maxX && treeMaxX >= minX
			&& treeMinY <= maxY && treeMaxY >= minY
			&& treeMinZ <= maxZ && treeMaxZ >= minZ;
	}
	this.collidesB = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
		return false;
	}
	this.collidesC = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
		return false;
	}
}
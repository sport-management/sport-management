//Ce fichier sert à informer le joueur à ce qu'il aurait dû faire pour préparer son saut de haies\\
//-------------Ce qu'il a donc informer dans le formulaire au début de la partie-----------------\\


let repasVeille = sessionStorage.getItem("repasVeille");
let petitDej = sessionStorage.getItem("petitDej");
let repasMidi = sessionStorage.getItem("repasDuMidi");
let chaussurePointe = sessionStorage.getItem("chaussurePointe");
let encas = sessionStorage.getItem("encas");
let toutBon=true;
//----------------------si il n'a pas eu bon sur le repas de la veille----------------------------------------\\
if (repasVeille != 20) {
    let i = Math.floor(Math.random() * Math.floor(2));
    let tab = ["Il vaut mieux éviter les aliments gras tels que la charcuterie ou bien des plats frits. Le repas doit être facile à digérer pour aider à la digestion.", "Il vaut mieux éviter les aliments épicés, les aliments panés et l'alcool la veille d'une épreuve. Le repas doit être facile à digérer pour faciliter le sommeil."]
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour le repas de la veille : "+tab[i];
    document.getElementById('conseil').appendChild(li);
    toutBon=false;
}
//----------------------si il n'a pas eu bon sur le petit déjeuner----------------------------------------\\
if (petitDej != 20) {
    
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour le petit déjeuner : Le petit déjeuner doit rester habituel pour éviter des intolérances. L'augmentation des féculents permet une bonne recharge glucidique.";
    document.getElementById('conseil').appendChild(li);
    toutBon=false;
}
//----------------------si il n'a pas eu bon sur le repas du midi----------------------------------------\\
if (repasMidi != 20) {
    let i = Math.floor(Math.random() * Math.floor(1));
    let tab=["Le dernier repas devra être terminé 3 heures avant l'effort. Il est préférable de s’alimenter en continu pendant les 2 dernières heures grâce à des collations, à intervalles réguliers. L’objectif est des garder le corps hydraté, tout en apportant une valeur énergétique."]
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour le repas avant l'épreuve : "+tab[i];
    document.getElementById('conseil').appendChild(li);
    toutBon=false;
}
//----------------------si il n'a pas mis les bonnes chaussures----------------------------------------\\
if (chaussurePointe != 20) {
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour les chaussures : Les chaussures à pointes permettent un gain de rapidité mais aussi un gain d'adhérence. Les pointes pour les sprinteurs sont placées au niveau de la plante de pied.";
    document.getElementById('conseil').appendChild(li);
    toutBon=false;
}
//----------------------si il a pris son repas 1 heure avant la course----------------------------------------\\
if (encas != 20) {
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour les encas : Il faut s'abstenir de manger ou de boire trop sucré une heure avant l'effort pour, éviter une hypoglycémie et conduire à la fringale de début de course.";
    document.getElementById('conseil').appendChild(li);
    toutBon=false;
}
//-----------------------si il a eu bon a toutes lese réponses----------------------------------------\\
if(toutBon){
    let li = document.createElement('li');
    li.innerHTML = "Vous n'avez fait aucune faute dans le formulaire";
    document.getElementById('conseil').appendChild(li);
}

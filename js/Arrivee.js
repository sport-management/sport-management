//-----------------------------Création de l'arrivée graphiquement-----------------------------------\\
function Arrivee(x,y,z){
    var self = this;
    this.mesh = new THREE.Object3D();
    var left = createCylinder(75, 75, 1200, 32,Colors.white, -1500, 300, 0);//poteau gauche
    var right = createCylinder(75, 75, 1200, 32, Colors.white, 1500, 300, 0);//poteau droit
    var top = createRectangle(300, 3100, 32, Colors.black, 0, 900, 0);//barre centrale
    top.rotation.z = Math.PI / 2;//on met la barre centrale à l'horizontale
    this.mesh.add(left);
    this.mesh.add(right);
    this.mesh.add(top);
    this.mesh.position.set(x,y,z)
    //-------------------------------ajout des collisions de l'arrivée--------------------------------\\
    this.collides = function(minX, maxX, minY, minZ, maxZ) {
        var meshMinX = self.mesh.position.x - this.scale * 250;
        var meshMaxX = self.mesh.position.x;
        var meshMaxY = self.mesh.position.y + this.scale * 1150;
        var meshMinZ = self.mesh.position.z - this.scale * 250;
        var meshMaxZ = self.mesh.position.z + this.scale * 250;
        return (meshMinX <= maxX && meshMaxX >= minX
            && meshMaxY >= minY
            && meshMinZ <= maxZ && meshMaxZ >= minZ)
    }
    this.collidesB = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesC = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
} 

//----------------------Création de la canette de sode graphiquement--------------------------------\\ 
function Canette(x, y, z) {

    var self = this;
    this.mesh = new THREE.Object3D();
    var forme = createCylinder(35, 35, 150, 32, Colors.red, 0, 0, 0);//poteau gauche
    var texte = createBox(20, 80, 30, Colors.white, 0, 20, 21);//poteau gauche
    this.mesh.add(forme);
    this.mesh.add(texte)
    this.mesh.position.set(x, y, z)

    //---------------------------ajout des collisions de la canettte-------------------------------------\\ 
    this.collides = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesB = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesC = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        var meshMinX = self.mesh.position.x - 125;
        var meshMaxX = self.mesh.position.x;
        var meshMaxY = self.mesh.position.y + 0.5*1150;
        var meshMinZ = self.mesh.position.z - 125;
        var meshMaxZ = self.mesh.position.z + 125;
        return (meshMinX <= maxX && meshMaxX >= minX
            && meshMaxY >= minY
            && meshMinZ <= maxZ && meshMaxZ >= minZ)
    }
}

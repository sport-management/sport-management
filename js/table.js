//-------------permet la création de tables sur le côté du marathon pour simuler des ravitaillements-------\\

function Table(x, y, z){

    var self = this;
    this.mesh = new THREE.Object3D();
    var pied1 = createCylinder(30, 30, 400, 32,Colors.black, 250, 0, -450);
    var pied2 = createCylinder(30, 30, 400, 32,Colors.black, -250, 0, 450);
    var pied3 = createCylinder(30, 30, 400, 32,Colors.black, 250, 0, 450);
    var pied4 = createCylinder(30, 30, 400, 32,Colors.black, -250, 0, -450);
    var planche = createBox(600, 10, 1000, Colors.grey, 0, 200, 0);
    this.mesh.add(pied1);
    this.mesh.add(pied2);
    this.mesh.add(pied3);
    this.mesh.add(pied4);
    this.mesh.add(planche);
    this.mesh.position.set(x, y, z)

    this.collides = function(minX, maxX, minY, maxY, minZ, maxZ) {
        var meshMinX = self.mesh.position.x - this.scale * 1;
        var meshMaxX = self.mesh.position.x;
        var meshMaxY = self.mesh.position.y + this.scale * 1;
        var meshMinZ = self.mesh.position.z - this.scale * 1;
        var meshMaxZ = self.mesh.position.z + this.scale * 1;
        return (meshMinX <= maxX && meshMaxX >= minX
            && meshMaxY >= minY
            && meshMinZ <= maxZ && meshMaxZ >= minZ)
    }
    this.collidesB = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesC = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
} 
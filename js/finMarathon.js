//Ce fichier sert à informer le joueur à ce qu'il aurait dû faire pour préparer son marathon\\
//-------------Ce qu'il a donc informer dans le formulaire au début de la partie-----------------\\


let semainePrec = sessionStorage.getItem("semainePrec");
let petitDejMarathon = sessionStorage.getItem("petitDejMarathon");
let entrainement = sessionStorage.getItem("entrainement");
let boissonAttente = sessionStorage.getItem("boissonAttente");
let ravitaillement = sessionStorage.getItem("ravitaillement");
let recuperation = sessionStorage.getItem("recuperation");
let toutBon = true;

//----------------------si il n'a pas eu bon sur la semaine précédente----------------------------------------\\
if (semainePrec != 20) {
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour la semaine précédante sur la nutrition : Un apport plus conséquent de glucide complexe qui permet d'optimiser le stockage au niveau du foie et des muscles. Les glucides servant de "+'"carburant" pour le corps.';
    document.getElementById('conseil').appendChild(li);
    toutBon = false;
}
//----------------------si il n'a pas eu bon sur le petit déjeuner du marathon----------------------------------------\\
if (petitDejMarathon != 20) {

    let li = document.createElement('li');
    li.innerHTML = "Conseil pour le petit déjeuner : Il est essentiel de recharger correctement les réserves de glucides et l'hydratation du corps avant la compétition pour pouvoir tenir jusqu'au bout.";
    document.getElementById('conseil').appendChild(li);
    toutBon = false;
}
//----------------------Conseil pour le ravitaillement----------------------------------------\\
if (ravitaillement != 20) {
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour le ravitaillement : Il est impératif de s'arrêter à chaque ravitaillement. Une perte d'un pour cent d'eau entraine une altération des performances de 10%. Privilégiez les gels énergétiques." ;
    document.getElementById('conseil').appendChild(li);
    toutBon = false;
}
//----------------------Conseil pour l'entrainement----------------------------------------\\
if (entrainement != 10) {
    let i = Math.floor(Math.random() * Math.floor(3));
    let tab = ["Courir un marathon ne s'improvise pas ! Il y a un risque de blessure si l'entrainement ne suit pas ","Les séances d'endurance permettent de préparer l'organisme à des efforts de plus en plus long. Il est important de varier le programme d’entraînements pour progresser.","Les séances de fractionnés permettent de travailler sur des zones d'intensité cardiovasculaire plus exigeantes. Cela permet de développer la puissance et la technique de course."]
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour l'entraînement : "+tab[i];
    document.getElementById('conseil').appendChild(li);
    toutBon=false;
}
//----------------------Conseil boisson d'attente----------------------------------------\\
if (boissonAttente != 10) {
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour la boisson d'attente : Le stress avant une course fait perdre des éléments essentiels au corps. Une boisson d'attente permet de garder un statut énergétique correct. Elle permet aussi d'éviter la baisse d'énergie en début de course.";
    document.getElementById('conseil').appendChild(li);
    toutBon = false;
}
//----------------------Conseil pour la récupération----------------------------------------\\
if (recuperation != 20) {
    let li = document.createElement('li');
    li.innerHTML = "Conseil pour la récupération : à la fin de l'épreuve, il est important de consommer des aliments sucrés simples comme des agrumes ou bien des barres de céréales. Cela va permettre de restaurer une partie des stocks énergétiques. Pour le repas qui suit, il faut privilégier un repas hyper-glucidique. Il permettra de restaurer les stocks glycogéniques.";
    document.getElementById('conseil').appendChild(li);
    toutBon = false;
}

//-----------------------si il a eu bon a toutes lese réponses----------------------------------------\\
if (toutBon) {
    let li = document.createElement('li');
    li.innerHTML = "Vous n'avez fait aucune faute dans le formulaire";
    document.getElementById('conseil').appendChild(li);
}


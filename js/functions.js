//---toutes les fonctions un peu complexe de THREEJS qui permettent de faire des formes particulières-\\

function createCylinder(radiusTop, radiusBottom, height, radialSegments,
    color, x, y, z) {
var geom = new THREE.CylinderGeometry(
radiusTop, radiusBottom, height, radialSegments);
var mat = new THREE.MeshPhongMaterial({
color: color,
flatShading: true
});
var cylinder = new THREE.Mesh(geom, mat);
cylinder.castShadow = true;
cylinder.receiveShadow = true;
cylinder.position.set(x, y, z);
return cylinder;
}
function sinusoid(frequency, minimum, maximum, phase, time) {
	var amplitude = 0.5 * (maximum - minimum);
	var angularFrequency = 2 * Math.PI * frequency;
	var phaseRadians = phase * Math.PI / 180;
	var offset = amplitude * Math.sin(
		angularFrequency * time + phaseRadians);
	var average = (minimum + maximum) / 2;
	return average + offset;
}
function createRectangle(hauteur, largeur, longueur,
    color, x, y, z) {
var geom = new THREE.BoxGeometry(hauteur, largeur,  longueur );
var mat = new THREE.MeshBasicMaterial({
color: color,
flatShading: true
});
var cube = new THREE.Mesh(geom, mat);
cube.castShadow = true;
cube.receiveShadow = true;
cube.position.set(x, y, z);
return cube;
}

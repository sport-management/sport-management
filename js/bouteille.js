//----------------------Création de la bouteille d'eau graphiquement--------------------------------\\ 

function Bouteille(x, y, z) {

    var self = this;
    this.mesh = new THREE.Object3D();
    var bas = createCylinder(30, 30, 100, 32, Colors.blue, 0, 0, 0);//poteau gauche
    var bouchon = createCylinder(15, 15, 20, 32, Colors.white, 0, 80, 0);
    this.mesh.add(bas);
    this.mesh.add(bouchon);
    this.mesh.position.set(x, y, z)
//---------------------------ajout des collisions de la bouteille-------------------------------------\\ 
    this.collides = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesC = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        return false;
    }
    this.collidesB = function (minX, maxX, minY, maxY, minZ, maxZ) {//ajout des collisions de la barriere
        var meshMinX = self.mesh.position.x - 125;
        var meshMaxX = self.mesh.position.x;
        var meshMaxY = self.mesh.position.y + 0.5*1150;
        var meshMinZ = self.mesh.position.z - 125;
        var meshMaxZ = self.mesh.position.z + 125;
        return (meshMinX <= maxX && meshMaxX >= minX
            && meshMaxY >= minY
            && meshMinZ <= maxZ && meshMaxZ >= minZ)
    }
}
